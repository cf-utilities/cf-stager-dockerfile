FROM ruby:2.2.4
ENV CI true
RUN git clone https://github.com/sstephenson/bats.git \
    && cd bats \
    && ./install.sh /usr/local \
    && cd - \
    && rm -rf bats
RUN curl --location -s https://www.jpluscplusm.com/jq-1.3-amd64 -o/usr/local/bin/jq && chmod u+x /usr/local/bin/jq
RUN wget "https://cli.run.pivotal.io/stable?release=debian64&version=6.21.0&source=github-rel" -O /tmp/cf.pkg
RUN dpkg -i /tmp/cf.pkg